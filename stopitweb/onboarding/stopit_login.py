
from django.views.generic import View
from django.conf import settings
from django.shortcuts import render, redirect
from django.template import loader, Context
from base64 import b64encode, b64decode
import requests, json, datetime
from onboarding.stopit_getcountries import get_countries, get_org_logininfo
import onboarding.stopit_messenger as messenger
import onboarding.stopit_constants as constants
from onboarding.stopit_contacts import get_contacts, get_resources
from django.utils.translation import LANGUAGE_SESSION_KEY
from django.utils.translation import to_locale, get_language
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.translation import ugettext as _
from stopit_logging import logging
from urlparse import urlparse


CHECK_TOC_FAILURE = 3
CHECK_TOC_ACCEPTED = 2
CHECK_TOC_NOT_ACCEPTED = 1

def openChat(request, times_called=0):
    params = {}
    keep_logged_in = request.session.get('keep_logged_in', 0)
    true_intellicode = request.session.get('user')
    if not true_intellicode:
        return render(request, 'accesscode.html')

    json_data = {"intellicode":true_intellicode, "serial_number":"website"}
    json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
    json_response = requests.post(settings.API_ENDPOINT_URL + '/v2/devices/webs/register', data=json.dumps({"data": json_data}), headers=json_headers)
    logging.debug('register 2 called with')
    logging.debug(json_data)

    json_object = json_response.json()
    logging.debug(json_object)
    logging.debug('Response Code: ' + str(json_response.status_code))
    json_object = json_response.json()

    if json_response.status_code == 200:
        if json_object.get('code') == '100000':
            data = json_object.get('data')
            if data:
                userInfo = data.get('user')
                orgInfo = data.get('org')
                chatInfo = data.get('chat')
                if userInfo:
                    if keep_logged_in:
                        request.session.set_expiry(settings.KEEP_LOGGED_IN_AGE)
                        request.session['keep_logged_in'] = 1
                    else:
                        request.session['keep_logged_in'] = 0
                        request.session.set_expiry(0)

                if chatInfo:
                    after_hours = False
                    if 'after_hour_messenger_message' in chatInfo:
                        after_hours = True
                        params['after_hours_on'] = after_hours
                        params['after_hours_message'] = chatInfo['after_hour_messenger_message']
                        params['is_chat_active'] = chatInfo['is_chat_active']

                if orgInfo:
                    request.session['session_org_id'] = orgInfo.get('org_id')
                    params['ORG_NAME'] = orgInfo.get('org_name')
                    params['LOGO_PATH'] = orgInfo.get('logo_path')
                    if orgInfo.get('incident_to_report_mgr') == 1:
                        params['report_manager_mandatory'] = 1
                    
                    # Check for disallow image upload
                    if orgInfo.get('disallow_upload_media') == 1:
                        params["disallow_upload_media"] = 1

                    # Check for org live not live
                    if not orgInfo.get('org_alive_flag'):
                        params["reason"] = "STOPit is not quite ready for use at your organization. " \
                                           "Please check back soon."
                        return render(request, 'disallow_incident.html', params)
                    # Check for recess
                    if orgInfo.get('is_in_recess') == 1:
                        params["is_in_recess"] = 1
                        params["reason"] = "Sorry, this feature is currently unavailable " \
                                           "because school is out of session."
                        return render(request, 'disallow_incident.html', params)
                get_contacts(request)
                get_resources(request)
                offlineMessages, offlineCount = get_offline_messages(true_intellicode)
                params['offlineMessages'] = offlineMessages
                params['offlineCount'] = offlineCount
                return render(request, 'incident.html', params)
            else:
                params["error_message"] = _("Invalid Access Code")
                return render(request, 'accesscode.html', params)
        else:
            logging.debug('did not get 100000')
            from time import sleep
            sleep(0.1)
            if times_called < 3:
                return openChat(request, times_called+1)
            else:
                params["error_message"] = _("Invalid Access Code")
                return render(request, 'accesscode.html', params)
    else:
        params["error_message"] = _("Invalid Access Code")
        return render(request, 'accesscode.html', params)


def login(request):
        params = {}
        logging.debug('login got called')
        access_code = request.session.get('access_code')
        logging.debug(access_code)
        keep_logged_in = request.session.get('keep_logged_in')
        json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
        try:
            if request.method == 'GET':
                if request.session.get('user'):
                    return openChat(request)
                else:
                    return logout(request)
            elif request.POST and 'accept' in request.POST:
                request.session['accepted_toc'] = True
                if request.session.get('user'):
                    return openChat(request)
                if access_code:
                    json_data = {"regis_code":access_code, "serial_number":"website"}
                    json_response = requests.post(settings.API_ENDPOINT_URL + '/v2/devices/webs/getIntellicodes', data=json.dumps({"data": json_data}), headers=json_headers)
                    json_object = json_response.json()
                    # process response for getIntellicode
                    if json_response.status_code == 200:
                        if json_object.get('code') == '100000':
                            data = json_object.get('data')
                            if data:
                                request.session['user'] = data.get('intellicode')
                                return openChat(request)

                    if not request.session.get('user'):
                        logout(request)
                        params["error_message"] = _("Invalid Access Code")
                        return render(request, 'accesscode.html', params)
                
            elif request.POST and 'decline' in request.POST:
                return logout(request)

            elif request.POST and 'no_accesscode' in request.POST:
                return render(request, 'options.html', params)
            else:
                return logout(request)


        except Exception,e:
            logging.debug(str(e))
            params["error_message"] = _("Invalid Access Code")
            return render(request, 'accesscode.html', params)


def setlangauage(request):
    params = {}

    if request.GET.get('language', 'en'):
        request.session[LANGUAGE_SESSION_KEY] = request.GET.get('language', 'en')
    else:
        request.session[LANGUAGE_SESSION_KEY] = 'en'
    return HttpResponseRedirect("/")


def logout(request):
        json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
        json_data = {"intellicode":request.session.get('user')}
        try:
            requests.post(settings.API_ENDPOINT_URL + '/v2/devices/unencrypted_logouts', data=json.dumps({"data": json_data}), headers=json_headers)
        except Exception as e:
            logging.error(str(e))

        # if user is already logged in, sign them out
        request.session.delete()
        if 'accepted_toc' in request.session:
            del request.session['accepted_toc']
        if 'user' in request.session:
            del request.session['user']
        request.session.modified = True
        params = {}
        return render(request, 'accesscode.html', params)


def loginviainfo(request):
        logging.debug('login via info got called')
        try:
            if request.method == "GET":
                return login(request, request.session.get('user', None), request.session.get('keep_logged_in', 0), True) 
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            last_name = request.POST['last_name']
            org_id = request.POST['org_id']
            roster_type = request.POST['roster_type']
            roster_id = request.POST['roster_id']
            keep_logged_in_value = request.POST.get('keep_logged_in', 0)
            logging.debug(keep_logged_in_value)
            if str(keep_logged_in_value) == str(1):
                keep_logged_in = True
                request.session['keep_logged_in'] = 1

            if roster_type.lower() != 'email':
                roster_type = "id"


            json_data = {'last_name' : last_name,
                         'serial_number': 'website',
                         'org_id': org_id,
                         'roster_type': roster_type,
                         'roster_id': roster_id}
            logging.debug(json_data)
            
            json_response = requests.post(settings.API_ENDPOINT_URL +'/v1/intellicodes/search', data=json.dumps(json_data), headers=json_headers)
            json_object = json_response.json()
            params = {}

            params["name"] = "success"
            logging.debug(json_object)
            if json_response.status_code == 200:
                if json_object.get('code')=='100000':
                    chatInfo = json_object['data']['chat']
                    if chatInfo:
                        after_hours = False
                        if 'after_hour_messenger_message' in chatInfo:
                            after_hours = True
                            params['after_hours_on'] = after_hours
                        params['is_chat_active'] = chatInfo['is_chat_active']

                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['intellicode']:
                        request.session['user'] = json_object['data']['user']['intellicode']
                    if json_object['data'] and json_object['data']['user'] and json_object['data']['user']['salt']:
                        request.session['salt'] = json_object['data']['user']['salt']
                    if json_object['data'] and json_object['data']['org'] and json_object['data']['org']['org_id']:
                        request.session['session_org_id'] = json_object['data']['org']['org_id']
                        params['ORG_NAME'] = json_object['data']['org']['org_name']

                    params['ORG_NAME'] = json_object['data']['org'].get('org_name', "org_name")
                    if json_object['data']['org'].get('incident_to_report_mgr', "0") == "1":
                        params['report_manager_mandatory'] = 1
                    # Check for org live not live
                    if json_object['data'] and json_object['data']['org'] \
                            and not json_object['data']['org']['org_alive_flag']:
                            params["reason"] = "STOPit is not quite ready for use at your organization. " \
                                               "Please check back soon."
                            return render(request, 'disallow_incident.html', params)

                    # Check for recess
                    if json_object['data'] and json_object['data']['org'] \
                            and json_object['data']['org']['is_in_recess']:
                        if str(json_object['data']['org']['is_in_recess']) == str(1):
                            params["is_in_recess"] = 1
                            params["reason"] = "Sorry, this feature is currently unavailable " \
                                               "because school is out of session."
                            return render(request, 'disallow_incident.html', params)

                    # Check for disallow image upload
                    if json_object['data'] and json_object['data']['org'] \
                            and json_object['data']['org']['disallow_upload_media']:
                        if str(json_object['data']['org']['disallow_upload_media']) == str(1):
                            params["disallow_upload_media"] = 1
                    offlineMessages, offlineCount = get_offline_messages(request.session['user'])
                    params['offlineMessages'] = offlineMessages
                    params['offlineCount'] = offlineCount
                    return render(request, 'incident.html', params)
                else:
                    return render(request, 'options.html', params)
            else:
                return render(request, 'options.html', params)
        except:
            params["name"] = "Django"
            return render(request, 'options.html', params)


def get_offline_messages_ajax(request):
    # continuous ajax call to get offline messages:
    get_offlinechats_json_data = {'intellicode': request.session.get('user') }
    json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
    try:
        get_offlinechats_json_response = requests.post(settings.API_ENDPOINT_URL+ '/chats/offlines/getuserofflinemessages', data=json.dumps(get_offlinechats_json_data), headers=json_headers)
        get_offlinechats_json_object = get_offlinechats_json_response.json()
    except Exception,e:
        logging.error(str(e))
        return HttpResponse(
                            json.dumps({'offMessagesCount' : 0 }),
                            status=200,
                            content_type='application/json')
    if get_offlinechats_json_response.status_code == 200:
        if get_offlinechats_json_object.get('code') == '100000':
            data = get_offlinechats_json_object.get('data', None)
            if data:
                offlineChats = data['offlineMessages']
                response_to_frontend = { 'offMessagesCount' : len(offlineChats) } 
                return HttpResponse(
                                    json.dumps(response_to_frontend),
                                    status=200,
                                    content_type='application/json')
    return HttpResponse(
                        json.dumps({'offMessagesCount' : 0 }),
                        status=200,
                        content_type='application/json')

def get_offline_messages(intellicode):
    # get offline messages before connecting to open fire:
    get_offlinechats_json_data = {'intellicode': intellicode }
    json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
    try:
        get_offlinechats_json_response = requests.post(settings.API_ENDPOINT_URL+ '/chats/offlines/getuserofflinemessages', data=json.dumps(get_offlinechats_json_data), headers=json_headers)
        get_offlinechats_json_object = get_offlinechats_json_response.json()
    except Exception,e:
        logging.error(str(e))
    if get_offlinechats_json_response.status_code == 200:
        if get_offlinechats_json_object.get('code') == '100000':
            data = get_offlinechats_json_object.get('data', None)
            if data:
                offlineChats = data['offlineMessages']
                return (offlineChats, len(offlineChats))
    return (None, 0)


def messenger_history(request, incident_id):
    history = {}
    if incident_id:
        chat_history_obj = messenger.get_chat_history(request, incident_id)
        if chat_history_obj.get('code') == '100000':
            chat_history = chat_history_obj.get('chat_history')
            for i in xrange(0,len(chat_history)):
                chat_entry = chat_history[i]
                # if chat_entry.get('sentDate'):
                #     chat_entry['sentDate'] = str(datetime.datetime.fromtimestamp(int(chat_entry['sentDate'])/1000))
                history[str(i)] = chat_entry

    # JSON object of chat history messages.
    return HttpResponse(json.dumps(history),content_type='application/json; charset=utf-8')


def flag_as_read(request, org_id, incident_id):
    jsonResponse = {}

    if request and org_id and incident_id:
        if request.method == 'GET':
            intellicode = request.session.get('user')
        else:
            intellicode = request.POST['intellicode']
        logging.debug('intellicode is ' + intellicode)

        json_headers = {
            'content-type': 'application/json',
            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)
        }
        json_data = {
            'intellicode':intellicode,
            'data': {
                'roomID': incident_id
            }
        }
        json_response = requests.post(settings.API_ENDPOINT_URL+ '/v4/intellicodes/webs/updatemsgs', data=json.dumps(json_data), headers=json_headers)
        jsonResponse = json_response.json()

    return HttpResponse(
        json.dumps(jsonResponse),
        content_type='application/json; charset=utf-8'
    )

def messenger_via_login(request):
        params = {}
        logging.debug('messenger login got called')
        multiple_conversations = False
        messenger_rooms = []

        try:

            if request.method == 'GET':
                intellicode = request.session.get('user')
            else:
                intellicode = request.POST['intellicode']

            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_data = {'intellicode':intellicode, 'serial_number': 'website'}
            logging.debug('intellicode is ' + intellicode)
            json_response = requests.post(settings.API_ENDPOINT_URL+ '/v2/devices/webs/register', data=json.dumps({"data":json_data}), headers=json_headers)
            params["name"] = "success"
            json_object = json_response.json()

            if json_response.status_code == 200 and json_object and json_object.get('code') == '100000' and json_object.get('data'):
                data = json_object['data']

                # check for existing conversations
                conversation_list_response = requests.post(
                    settings.API_ENDPOINT_URL + '/v4/intellicodes/webs/rooms',
                    data=json.dumps(json_data),
                    headers=json_headers
                )
                conversation_list = conversation_list_response.json()

                if conversation_list_response.status_code == 200 and \
                        conversation_list and conversation_list.get('code') == '100000' \
                        and conversation_list.get('data'):
                    messenger_rooms = conversation_list['data'].get('rooms', [])
                    if messenger_rooms and len(messenger_rooms) > 1:
                        multiple_conversations = True

                chatHistoryJSON = {}
                chatConnectionDetail = {}
                if data['chat'] and data['chat']['self']:
                    chat_username = data['chat']['self'].get('username')
                    chat_password = data['chat']['self'].get('password')
                if data['chat'] and data['chat']['admin']:
                    after_hours = False
                    if 'after_hour_messenger_message' in data['chat']:
                        after_hours = True
                    chatHistoryJSON['after_hours_on'] = after_hours
                    chatHistoryJSON['is_chat_active'] = data['chat']['is_chat_active']
                if data['org']:
                    chatHistoryJSON['org_id'] = data['org'].get('org_id')
                    chatHistoryJSON['is_in_recess'] = data['org'].get('is_in_recess')
                if json_object['data']['user'] and json_object['data']['user']['intellicode']:
                   request.session['user'] = json_object['data']['user']['intellicode']
                   request.session['ORG_NAME'] = json_object['data']['org']['org_name']
                   chatConnectionDetail = messenger.get_messenger_session(request, chat_username, chat_password)

                chatHistoryJSON['messenger_host'] = chatConnectionDetail.get('messenger_host')
                chatHistoryJSON['messenger_jid'] = chatConnectionDetail.get('messenger_jid')
                chatHistoryJSON['messenger_rid'] = chatConnectionDetail.get('messenger_rid')
                chatHistoryJSON['messenger_sid'] = chatConnectionDetail.get('messenger_sid')
                chatHistoryJSON['bind_proxy'] = messenger.get_bind_proxy_from_ip(chatConnectionDetail.get('instance_ip'))
                chatHistoryJSON['messenger_assets'] = settings.CDN_HOST
                chatHistoryJSON['messenger_room_domain'] = settings.MESSENGER_ROOM_DOMAIN
                chatHistoryJSON['display_conversation_list'] = multiple_conversations
                chatHistoryJSON['messenger_rooms'] = messenger_rooms

                return HttpResponse(json.dumps(chatHistoryJSON),content_type='application/json; charset=utf-8') # JSON object of chat history messages.

            else:
                params["error_message"] = _("Invalid Access Code")
                return render(request, 'options.html', params)

        except Exception, e:
            logging.error(str(e))
            params["error_message"] = _("Invalid Access Code")
            return render(request, 'options.html', params)


def get_toc(request):
    logging.debug('Get TOC got called')
    template_param = {}
    if 'accepted_toc' in request.session and request.session['accepted_toc']:
        template_param['accepted_toc'] = True

    url = request.META.get('HTTP_REFERER')
    parsed_uri = urlparse( url )
    dom = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    url_postfix = dom[-3:-1]
    domain = constants.DOMAINS.get(url_postfix) if url_postfix in constants.DOMAINS else 'com'
    logging.debug(domain)
    template_param['country_code'] = constants.COUNTRIES.get(url_postfix) if constants.COUNTRIES.get(url_postfix) else 'US'
    if request.POST:
        if not request.POST.get('no_accesscode'):
            request.session['access_code'] = request.POST.get('intellicode')
        else:
            request.session['no_accesscode'] = True
        request.session['keep_logged_in'] = request.POST.get('keep_logged_in')
    
    try:
        url = settings.CMS_PROTOCOL + settings.CMS_HOST + str(domain) +  '/app-terms-of-use-raw/'
        http_response = requests.get(url)
        http_content = http_response.content
        template_param["tos"] = http_content

        return render(request, 'terms_of_use.html', template_param)
    except Exception as e:
        logging.error(str(e))
        return render(request, 'terms_of_use.html', template_param)


def check_toc_acceptance(request):
    logging.debug('checking toc acceptance')
    template_param = {}
    try:    
        access_code = request.session.get('user')
            
   
        json_headers = {'content-type': 'application/json',
                        'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

        json_response = requests.get(settings.API_ENDPOINT_URL +'/v1/intellicodes/' + access_code + '/checktos',
                                     headers=json_headers)
        json_object = json_response.json()
        logging.debug(json_object)

        if json_response.status_code == 200:
            if json_object.get('code') == '100000':
                if json_object['data']:
                    accepted_date = json_object['data']['accepted_tos_date']
                    if accepted_date is None:
                        return CHECK_TOC_NOT_ACCEPTED # not accepted
                    else:
                        return CHECK_TOC_ACCEPTED # accepted
        return CHECK_TOC_FAILURE
        # some kind of error
    except Exception, e:
        logging.error(str(e))
        return CHECK_TOC_FAILURE


def check_tos_for_logininfo(request):
        logging.debug('check tos via info got called')
        try:
            if request.method == "GET":
                return login(request, request.session.get('user', None), request.session.get('keep_logged_in', 0), True) 
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' +
                                                                                 b64encode(settings.API_AUTH_NAME+':'
                                                                                           + settings.API_AUTH_PASS)}

            org_type = request.POST.get('org_type', 1)
            country_code = request.POST['country_code']
            state_code = request.POST['state_code']
            org_name = request.POST['org_name']

            last_name = request.POST['last_name']
            org_id = request.POST['org_id']
            roster_type = request.POST['roster_type']
            roster_id = request.POST['roster_id']

            keep_logged_in_value = request.POST.get('keep_logged_in', 0)

            keep_logged_in = False

            if str(keep_logged_in_value) == str(1):
                keep_logged_in = True

            if roster_type.lower() != 'email':
                roster_type = "id"

            json_data = {'last_name' : last_name,
                         'serial_number': 'website',
                         'org_id': org_id,
                         'roster_type': roster_type,
                         'roster_id': roster_id}
            logging.debug(json_data)

            json_response = requests.post(settings.API_ENDPOINT_URL + '/v1/intellicodes/searchwithtos',
                                          data=json.dumps(json_data), headers=json_headers)
            json_object = json_response.json()

            logging.debug(json_object)
            if json_response.status_code == 200:
                if json_object.get('code') == '100000':
                    if json_object['data']:
                        accepted_date = json_object['data']['accepted_tos_date']
                        intellicode = json_object['data']['intellicode']
                        if accepted_date is not None:
                            request.session['user'] = intellicode
                            return login(request, intellicode, keep_logged_in, accepted_date)
                        else:
                            request.session['user'] = intellicode
                            return get_toc(request, intellicode, keep_logged_in)
                    else:
                        return get_org_logininfo(request,
                                                 org_name=org_name,
                                                 error_message=_("Sorry! We couldn't find you. Please try again"))
                else:
                    return get_org_logininfo(request,
                                             org_name=org_name,
                                             error_message=_("Sorry! We couldn't find you. Please try again"))
            else:
                return get_org_logininfo(request,
                                         org_name=org_name,
                                         error_message=_("Sorry! We couldn't find you. Please try again"))
        except Exception, e:
            logging.error(str(e))
            return get_org_logininfo(request,
                                     org_name=org_name,
                                     error_message=_("Sorry! We couldn't find you. Please try again"))


def expire_session(request):

    if request.GET.get("expire") == "1":
        logout(request)
        # expire the session
        request.session.set_expiry(0)
        request.session.flush()
    else:
        request.session.set_expiry(settings.KEEP_LOGGED_IN_AGE)
    return HttpResponse(
            json.dumps({}),
            status=200,
            content_type='application/json'
        )
