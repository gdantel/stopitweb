from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from datetime import date, datetime, timedelta
import random, httplib, binascii, urllib, requests
import hashlib, json
from base64 import b64decode, b64encode
from onboarding.stopit_chat_client import chat_client
from django.views.decorators.csrf import csrf_exempt
from stopit_logging import logging

# Messenger constants
MESSENGER_URL = settings.MESSENGER_HTTP_PROTOCOL + settings.MESSENGER_HOST + ':' + str(settings.MESSENGER_HTTP_PORT) + '/http-bind/'
MESSENGER_HOST = settings.MESSENGER_HOST
API_ENDPOINT = settings.API_ENDPOINT_URL
PAGE_SIZE = 50


def get_messenger_session(request, chat_username, chat_password):
    logging.debug(' --------------- in get_messenger_session')

    response_data = {
        'messenger_url': MESSENGER_URL,
        'messenger_host': MESSENGER_HOST,
    }
    if chat_username and chat_password:
        messenger_client = chat_client(chat_username, chat_password)
        messenger_client.bosh_connect(request)

        response_data['messenger_rid'] = messenger_client.rid
        response_data['messenger_sid'] = messenger_client.sid
        response_data['messenger_jid'] = messenger_client.jabberid
        response_data['instance_ip'] = messenger_client.instance_ip

    return response_data


def get_stanza_id(stanza):
   return stanza[stanza.find("id=")+4:stanza.find("\" ")] 


def get_chat_history(request,incident_id = None):
    intellicode = request.session.get('user')
    chatHistoryURL = API_ENDPOINT+"/v4/intellicodes/webs/getChatHistory"
    response_to_frontend = {}

    if intellicode and incident_id:
        data = {
            "intellicode": str(intellicode),
            "data": {
                "roomID": int(incident_id),
            },
        }
        headers = {"content-type": "application/json",
                  "Authorization": "Basic " + b64encode(settings.API_AUTH_NAME+":"+settings.API_AUTH_PASS)}
        try:
            json_response = requests.post(chatHistoryURL, data=json.dumps(data), headers=headers)
            json_object = json_response.json()
            #test if response is bad
            if json_object['code'] != "100000":
                return {
                    'code': json_object['code'],
                    'message': json_object['message']
                }
            response_to_frontend['code'] = json_object['code']
            historyList = []
            for message in json_object['data']['read']:
                historyList.append({'id': get_stanza_id(message['stanza']), 'sentDate': str(message['sentDate']), 'body': message['body'], 'fromJID': message['fromJID'], 'toJID': message['toJID']})
            for message in json_object['data']['unread']:
                historyList.append({'id': get_stanza_id(message['stanza']), 'sentDate': str(message['sentDate']), 'body': message['body'], 'fromJID': message['fromJID'], 'toJID': message['toJID']})
            response_to_frontend['chat_history'] = historyList
        except Exception, e:
            response_to_frontend = {
                'code': 200000,
                'message': e[0],
            }

        return response_to_frontend


def get_bind_proxy_from_ip(ip = None):
    bind_proxy = '/http-bind/'
    if ip:
        for p in settings.HTTP_BIND_PROXIES:
            if p.get('ip') == ip:
                bind_proxy = p.get('bind_proxy')
                break

    return bind_proxy
