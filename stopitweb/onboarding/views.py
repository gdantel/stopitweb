from django.views.generic import View
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.utils.translation import  ugettext as _
from django.conf import settings
from django.http import HttpResponse
import requests, json

class Index(View):
    def get(self, request):

        params = {'name': 'django'}

        try:
            if request.method == 'GET':
                intellicode = request.session.get('user')
                if intellicode is not None:
                    return redirect('/login/')
        except:
            params['error_message'] = ""
            return render(request, 'accesscode.html', params)
        return render(request, 'accesscode.html', params)


class OptionsView(TemplateView):
    template_name = "options.html"



class accesscode(TemplateView):
    template_name = "accesscode.html"



class findcountry(TemplateView):
    template_name = "findcountry.html"



class incident(TemplateView):
    template_name = "incident.html"
    
    def get_context_data(self, **kwargs):
        context = super(incident, self).get_context_data(**kwargs)
        return context


class language(TemplateView):
    template_name = "language.html"


class help(TemplateView):
    template_name = "help.html"


class help_incident(TemplateView):
    template_name = "help_incident.html"
    help_img_uri = "assets/img/submit_incident_help.png"

    def get(self, request):
        context = self.get_context_data()
        if request.LANGUAGE_CODE == 'ja':
            self.help_img_uri = "assets/img/submit_incident_help_ja.png"
        else:
            self.help_img_uri = "assets/img/submit_incident_help.png"
        context['help_img_uri'] = self.help_img_uri
        return super(TemplateView, self).render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(help_incident, self).get_context_data(**kwargs)
        return context


class help_contact_list(TemplateView):
    template_name = "help_contact_list.html"


class help_edit_contact(TemplateView):
    template_name = "help_edit_contact.html"

def random_access_code(request, token = None):
    template_name = "random_access_code.html"

    params = {}
    if token:
        api_url_path = '/rosterless/accesscodeurl/' + str(token)
        json_headers = {
            'content-type': 'application/json'
        }
        json_response = requests.get(settings.API_ENDPOINT_URL + api_url_path, headers=json_headers)

        if json_response:
            json_object = json_response.json()
            if json_object and json_object.get('data'):
                params["token"] = token
                params["org_name"] = json_object['data']['org_name']

    return render(request, 'random_access_code.html', params)


def get_random_access_code(request, token = None):
    json_out = {
        'code': 100005,
        'message': _('An error has occurred.'),
    }
    if token:
        api_url_path = '/rosterless/accesscodeurl/' + str(token)
        json_headers = {
            'content-type': 'application/json'
        }
        json_response = requests.post(settings.API_ENDPOINT_URL + api_url_path, headers=json_headers)

        if json_response:
            json_object = json_response.json()
            if json_object and json_object.get('data'):
                json_out['code'] = 100000
                if json_object.get('message'):
                    json_out['message'] = json_object['message']
                json_out["data"] = json_object['data']

    return HttpResponse(
        json.dumps(json_out),
        status=200,
        content_type='application/json'
    )
