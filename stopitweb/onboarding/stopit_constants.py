DOMAINS = {
    'com': 'com',
    'au': 'com.au',
    'nz': 'co.nz',
    'za': 'co.za',
    'jp': 'jp',
}
COUNTRIES = {
    'com': 'US',
    'au': 'AU',
    'nz': 'NZ',
    'jp': 'JP',
}
