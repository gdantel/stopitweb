from django.conf import settings
from django import template
from ConfigParser import ConfigParser

register = template.Library()

@register.tag(name='captureas')
def do_captureas(parser, token):
    try:
        tag_name, args = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError("'captureas' node requires a variable name.")
    nodelist = parser.parse(('endcaptureas',))
    parser.delete_first_token()
    return CaptureasNode(nodelist, args)

class CaptureasNode(template.Node):
    def __init__(self, nodelist, varname):
        self.nodelist = nodelist
        self.varname = varname

    def render(self, context):
        output = self.nodelist.render(context)
        context[self.varname] = output
        return ''

@register.simple_tag
def cache_buster(filepath):
    deploy_version = ''
    try:
        deploy = ConfigParser()
        deploy.readfp(open(settings.DEPLOY_VERSION_FILE))
        deploy_version = '?v=' + deploy.get('deploy', 'version')
    except Exception as e:
        pass
    return settings.CDN_HOST_PROTOCOL + settings.CDN_HOST + filepath + deploy_version
