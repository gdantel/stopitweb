from django.views.generic import View
from django.shortcuts import render
from django.conf import settings
from base64 import b64encode
import requests, json
from django.utils.translation import ugettext as _
from stopit_logging import logging


def get_contacts(request):
        try:
            json_headers = {'content-type': 'application/json',
                            'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            intellicode = request.session['user']

            json_response = requests.get(settings.API_ENDPOINT_URL +'/v1/intellicodes/' + intellicode + '/contacts?no_encryption=1', headers=json_headers)
            json_object = json_response.json()
            if json_response.status_code == 200:
                if json_object.get('code') == '100000':
                    if json_object['data'] and json_object['data']['contacts']:
                        request.session['contacts'] = json_object['data']['contacts']

            return None
        except:
            return None


def get_resources(request):
    logging.debug('Get Resources got called')
    true_intellicode = request.session.get('user')
    if not true_intellicode:
        return render(request, 'accesscode.html')

    json_data = {"intellicode": true_intellicode, "serial_number": "website"}
    jai_data = json.dumps({"data": json_data})
    try:
        json_headers = {'content-type': 'application/json',
                        'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME + ':' + settings.API_AUTH_PASS)}

        json_response = requests.post(settings.API_ENDPOINT_URL + '/v2/devices/webs/gethelps', data=json.dumps({"data":json_data}), headers=json_headers)
        json_object = json_response.json()
        if json_response.status_code == 200:
            if json_object.get('code') == '100000':
                if json_object['data'] and json_object['data']['crisis_center']:
                    request.session['gethelp_resources'] = json_object['data']['crisis_center']
        return None
    except:
        return None

def show_contacts(request):
    logging.debug('Show contacts got called')
    try:
        if 'user' not in request.session:
            params= {"error_message": _("Invalid Access Code")}
            return render(request, 'options.html', params)
    finally:
        get_contacts(request)

    params = {}
    params["contacts"] = request.session['contacts']
    return render(request, 'contact_list.html', params)


def show_contact(request):
    logging.debug('Show contacts got called')
    try:
        if 'user' not in request.session:
            params= {"error_message": _("Invalid Access Code")}
            return render(request, 'options.html', params)
    finally:
        get_contacts(request)

    try:
        contact_id = request.GET.get('contact_id', 0)
    except:
        contact_id = request.POST.get('contact_id', 0)

    params = {}

    contacts = request.session['contacts']
    for contact in contacts:
        if str(contact['user_id']) == str(contact_id):
            params["CURRENT_CONTACT"] = contact

    if str(contact_id) == str(0):
        params["ADD_MODE"] = 1
    else:
        params["ADD_MODE"] = 0

    return render(request, 'edit_contact.html', params)


def save_contact(request):
    logging.debug('Save contacts got called')
    try:
        intellicode = request.session.get('user')

        user_id = request.POST.get('user_id', 0)
        name = request.POST.get('name')
        mobile_phone = request.POST.get('mobile_phone')
        email = request.POST.get('email')
        is_get_help = request.POST.get('is_get_help', 'N')
        is_trusted = request.POST.get('is_trusted', 'N')
        is_one_touch = request.POST.get('is_one_touch', 'N')


        if str(user_id) == str(0):
            json_data = {
                     'name': name,
                     'mobile_phone': mobile_phone,
                     'email': email,
                     'is_get_help': is_get_help,
                     'is_trusted': is_trusted,
                     'is_one_touch': is_one_touch,
                     }

            final_data = {'no_encryption': 1, 'data': json_data}
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            api_url = settings.API_ENDPOINT_URL+ '/v1/intellicodes/' + str(intellicode) + "/contacts"
            json_response = requests.post(api_url, data=json.dumps(final_data), headers=json_headers)
        else:
            json_data = {'user_id': user_id,
                     'name': name,
                     'mobile_phone': mobile_phone,
                     'email': email,
                     'is_get_help': is_get_help,
                     'is_trusted': is_trusted,
                     'is_one_touch': is_one_touch,
                     }

            final_data = {'no_encryption': 1, 'data': json_data}
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}

            api_url = settings.API_ENDPOINT_URL+ '/v1/intellicodes/' + str(intellicode) + "/contacts/" + str(user_id)
            for key, value in request.POST.iteritems():
                logging.debug(key)
                logging.debug(value)

            if 'save' not in request.POST:
                json_response = requests.delete(api_url, data=json.dumps(final_data), headers=json_headers)
            else:
                json_response = requests.put(api_url, data=json.dumps(final_data), headers=json_headers)

        get_contacts(request)
        params = {}
        params["contacts"] = request.session['contacts']
        return render(request, 'contact_list.html', params)
    except:
        return show_contacts(request)


