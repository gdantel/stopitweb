﻿from django.conf import settings
import random, httplib, binascii, uuid
from stopit_logging import logging

class chat_client:
    def __init__(self, jabberid = None, password = None):
        if jabberid and password:
            self.rid = random.randint(0, 10000000) + 1000000000
            if '@' in jabberid:
                jabberid = jabberid[:str(jabberid).index('@')]
            self.username = jabberid
            self.jabberid = jabberid + '@' + settings.MESSENGER_HOST
            self.password = password
            self.cookie = None
            self.raw_cookie = None
            self.instance_ip = None

            self.authid = None
            self.challenge_key = None
            self.sid = None
            self.logged_in = False
            self.headers = {
                "Content-type": "text/xml; charset=utf-8",
                "Accept": "text/xml",
                "Connection": "keep-alive",
                "Keep-Alive": 900,
            }
        else:
            self = None

    def bosh_connect(self, request):
        logging.error("bosh_connect start")
        connect_xml = "<body "\
            + "rid='" + str(self.rid) + "' "\
            + "xmlns='http://jabber.org/protocol/httpbind' "\
            + "to='" + settings.MESSENGER_HOST + "' "\
            + "xml:lang='en' wait='60' hold='1' content='text/xml; charset=utf-8' ver='1.6' xmpp:version='1.0' xmlns:xmpp='urn:xmpp:xbosh'/>"

        # get session
        response = self.send_bosh_xml(connect_xml)
        response_content = response.get('text')
        response_obj = self.parse_response(response_content)

        # get auth token
        if response_obj.get('sid'):
            logging.error("got sid")
            logging.error(response_obj.get('sid'))
            self.instance_ip = response.get('instance')
            self.rid = self.rid + 1
            self.sid = response_obj['sid']
            if not response_obj['authid']:
                self.authid = response_obj['sid']

            auth_str = ""
            auth_str += "\000"
            #auth_str += self.username.encode('utf-8')
            auth_str += self.jabberid.encode('utf-8')
            auth_str += "\000"
            try:
                auth_str += self.password.encode('utf-8').strip()
            except UnicodeDecodeError:
                auth_str += self.password.decode('latin1').encode('utf-8').strip()
            get_session_xml = "<body "\
                    + "rid='" + str(self.rid) + "' "\
                    + "xmlns='http://jabber.org/protocol/httpbind' "\
                    + "sid='" + self.sid + "'>"\
                    + "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='PLAIN'>"\
                    + binascii.b2a_base64(auth_str).replace('\n','')\
                    + "</auth></body>"
            response = self.send_bosh_xml(get_session_xml).get('text')
            response_obj = self.parse_response(response)

        if response and 'success' in response:
            self.rid = self.rid + 1
            ping_server_xml = "<body "\
                    + "xmlns='http://jabber.org/protocol/httpbind' "\
                    + "rid='" + str(self.rid) + "' "\
                    + "sid='" + str(self.sid) + "' "\
                    + "xml:lang='en' "\
                    + "to='" + settings.MESSENGER_HOST + "' "\
                    + "xmpp:restart='true' "\
                    + "xmlns:xmpp='urn:xmpp:xbosh' "\
                    + "content='text/xml; charset=utf-8' />"
            response = self.send_bosh_xml(ping_server_xml).get('text')

        # bind iq
        if response and '<bind' in response:
            self.rid = self.rid + 1
            bind_iq_xml = "<body "\
                    + "rid='" + str(self.rid) + "' "\
                    + "xmlns='http://jabber.org/protocol/httpbind' "\
                    + "sid='" + self.sid + "'>"\
                    + "<iq type='set' id='_bind_auth_2' xmlns='jabber:client'>"\
                    + "<bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'>"\
                    + "<resource>" + self.sid + "</resource>"\
                    + "</bind></iq></body>"
            response = self.send_bosh_xml(bind_iq_xml).get('text')
        
        # bind session
        if response and '<bind' in response and '_bind_auth_2' in response:
            self.rid = self.rid + 1
            bind_session_xml = "<body "\
                    + "rid='" + str(self.rid) + "' "\
                    + "xmlns='http://jabber.org/protocol/httpbind' "\
                    + "sid='" + self.sid + "'>"\
                    + "<iq type='set' id='_bind_auth_2' xmlns='jabber:client'>"\
                    + "<session xmlns='urn:ietf:params:xml:ns:xmpp-session'/>"\
                    + "</iq></body>"
            response_cookie = self.send_bosh_xml(bind_session_xml).get('cookie')


    def parse_response(self, response):
        response_object = {}
        logging.error("parse response")
        temp = None
        if response and ' ' in response:
            response_array = response.split(' ')
            for item in response_array:
                logging.error(item)
                if '=' in item:
                    item_array = item.split('=')
                    if len(item_array) > 0:
                        response_object[item_array[0]] = item_array[1].replace('"','')
        return response_object

    def send_bosh_xml(self, xml_string = None):
        import requests

        response_data = {}
        cookie = None
        if xml_string:
            s = requests.session()
            try:
                http_bind_url = settings.MESSENGER_HTTP_PROTOCOL + settings.MESSENGER_HOST + ':' + str(
                    settings.MESSENGER_HTTP_PORT) + '/http-bind/'
                if self.cookie:
                    response = s.post(http_bind_url, data=xml_string, headers=self.headers, cookies=self.cookie)
                else:
                    response = s.post(http_bind_url, data=xml_string, headers=self.headers)

                    if response.cookies:
                        cookie = response.cookies
                        raw_cookie = requests.utils.dict_from_cookiejar(s.cookies)

                if not self.cookie:
                    logging.error("get cookie")
                    self.cookie = cookie

                if response.status_code == 200:
                    response_data['text'] = response.text
                    response_data['cookie'] = response.cookies
                    if response.headers and response.headers.get('stopit_httpbindaddress'):
                        response_data['instance'] = response.headers['stopit_httpbindaddress']

            except Exception as e:
                logging.error('send_bosh_xml: ' + str(e))

        return response_data

    def get_messages(self, sid = None):
        # send message
        response = None
        get_message_xml = "<body rid='"+ str(self.rid) + "' "\
            + "sid='" + self.sid + "' " \
            + "xmlns='http://jabber.org/protocol/httpbind'/>"

        response = self.send_bosh_xml(get_message_xml.encode('utf-8'))
        logging.debug(response)
        response_obj = self.parse_response(response)

        return response
