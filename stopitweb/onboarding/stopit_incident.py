from django.shortcuts import render
from django.forms import Form
from django.conf import settings
from django.http import HttpResponse
from base64 import b64encode
from django.utils.translation import ugettext as _

import boto
import boto.s3.connection
from boto.s3.key import Key
import requests
import json
import uuid
from stopit_logging import logging
import socket


def connect_to_s3():
    if socket.gethostname()[:5].lower() == 'sitcb':
        s3_connection = boto.connect_s3(
            aws_access_key_id=settings.AWS_S3_ACCESS_KEY,
            aws_secret_access_key=settings.AWS_S3_SECRET_KEY,
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
        )
    else:
        s3_connection = boto.connect_s3()
    return s3_connection


def get_incident_upload_path(org_id=None, incident_id=None):
    upload_path = None
    if org_id is not None and incident_id is not None:
        try:
            upload_path = 'orgs/' + str(org_id) + '/incidents/' + str(incident_id) + '/attachments/'
            logging.debug(upload_path)
        except:
            pass
    return upload_path


def push_to_s3(bucket_name=None, file_path=None, file_name=None, org_id=None, incident_id=None):
    response_to_frontend = {
        'message': 'Upload failed',
        'data': {}
    }
    if file_path:
        if not bucket_name:
            bucket_name = settings.AWS_S3_BUCKET
        conn = connect_to_s3()
        s3_bucket = conn.get_bucket(bucket_name)

        try:
            s3_object = Key(s3_bucket)
            upload_file_path = get_incident_upload_path(org_id, incident_id)

            s3_object.key = upload_file_path + file_name
            s3_object.set_contents_from_filename(file_path)
            response_to_frontend['data']['file_name'] = s3_object.key
            response_to_frontend['data']['incident_id'] = incident_id
            response_to_frontend['message'] = 'ok'
        except Exception, e:
            pass

    return HttpResponse(
        json.dumps(response_to_frontend),
        status=200,
        content_type='application/json'
    )


def api_submit(remote_api_url, key_id=None, json_object=None, default_status_message='Action failed'):
    response_to_frontend = {
        'message': default_status_message
    }
    try:
        # changing no_encryption to 1 will turn off messenger in docit for this incident
        payload = {'no_encryption': 0, 'data': json_object}
        json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
        if key_id:
            logging.debug('API PUT URL: ' + remote_api_url)
            json_response = requests.put(remote_api_url, data=json.dumps(payload), headers=json_headers)
        else:
            logging.debug('API POST URL: ' + remote_api_url)
            json_response = requests.post(remote_api_url, data=json.dumps(payload), headers=json_headers)

        logging.debug('Response Code: ' + str(json_response.status_code))
        json_object = json_response.json()

        if json_response.status_code == 200:
            if json_object.get('code'):
                response_to_frontend['code'] = json_object['code']
            if json_object.get('message'):
                response_to_frontend['message'] = json_object['message']
            if json_object.get('data'):
                response_to_frontend['data'] = json_object['data']

            return HttpResponse(
                json.dumps(response_to_frontend),
                status=json_response.status_code,
                content_type='application/json'
            )
    except:
        pass

    return HttpResponse(
        json.dumps(response_to_frontend),
        status=200,
        content_type='application/json'
    )


def upload_to_s3(request, upload_api_url, incident_id=None):
    if request.FILES:
        logging.debug(' --------------- in upload_action:FILE_UPLOAD')

        form = Form(request.POST, request.FILES)
        if form.is_valid():
            aws_json_response = {}
            json_data = {}
            session_org_id = request.session['session_org_id']
            logging.debug("checking for attached files")
            upload_file_path = get_incident_upload_path(session_org_id, incident_id)
            for uploaded_file in request.FILES.getlist('attachedfiles'):    
                json_data['attachments'] = []
                attachment = {
                    'url': upload_file_path + uploaded_file.name,
                    'filename': uploaded_file.name,
                }
                json_data['attachments'].append(attachment)
                upload_response = api_submit(upload_api_url, incident_id, json_data)
                logging.debug('upload to s3 response for incident ID ' + str(incident_id) + ': ', upload_response)

                random_file_prefix = str(uuid.uuid4())
                full_path_file_name = settings.FILE_UPLOAD_PATH + '/' + random_file_prefix + '_' + uploaded_file.name
                logging.debug(full_path_file_name)

                destination = open(full_path_file_name, 'wb+')
                for chunk in uploaded_file.chunks():
                    destination.write(chunk)
                destination.close()

                aws_response = push_to_s3(
                    settings.AWS_S3_BUCKET,
                    full_path_file_name,
                    uploaded_file.name,
                    session_org_id,
                    incident_id
                )


                # remove temporary uploaded file after encoding
                try:
                    import os
                    os.remove(full_path_file_name)
                except:
                    pass
            return upload_response
    return None


def fileincident(request):
    template_params = {}
    logging.debug('incident submission got called')

    try:
        intellicode = request.session.get('user')
        params = {}

        try:
            # Check if the school is live, user is enabled and school is not on vacation
            json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
            json_data = {
                'intellicode':intellicode,
                'serial_number':'website',
            }
            json_response = requests.post(settings.API_ENDPOINT_URL + '/v2/devices/webs/register', data=json.dumps({"data":json_data}), headers=json_headers)
            json_object = json_response.json()
            logging.debug('Response Code: ' + str(json_response.status_code))
            json_object = json_response.json()

            if json_response.status_code == 200:
                if json_object.get('code') == '100000':

                    # Check for org live not live
                    if json_object['data'] and json_object['data']['org'] \
                            and not json_object['data']['org']['org_alive_flag']:
                            params["reason"] = "STOPit is not quite ready for use at your organization. " \
                                               "Please check back soon."
                            return render(request, 'disallow_incident.html', params)

                    # Check for recess
                    if json_object['data'] and json_object['data']['org'] \
                            and json_object['data']['org']['is_in_recess']:
                        if str(json_object['data']['org']['is_in_recess']) == str(1):
                            params["reason"] = "Sorry, this feature is currently unavailable " \
                                               "because school is out of session."
                            return render(request, 'disallow_incident.html', params)

                else:
                    params["error_message"] = _("Invalid Access Code")
                    return render(request, 'accesscode.html', params)
            else:
                params["error_message"] = _("Invalid Access Code")
                return render(request, 'accesscode.html', params)
        except:
            params["error_message"] = _("Invalid Access Code")
            return render(request, 'accesscode.html', params)

        notes = {'data': request.POST['notes']}
        is_anonymous = request.POST.get('is_anonymous', 0)
        notesarray = [notes]

        contactsarray = request.POST.getlist('report_contact')

        contactsdataArray = []
        for contact in contactsarray:
            dictuser = {'user_id': contact}
            contactsdataArray.append(dictuser)

        json_data = {
            'is_anonymous': is_anonymous,
            'incident_category_id': is_anonymous,
            'notes': notesarray,
            'contacts': contactsdataArray,
            'source': settings.INCIDENT_SOURCE_ID,
        }

        final_data = {'no_encryption': 0, 'data': json_data}

        json_headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + b64encode(settings.API_AUTH_NAME+':'+settings.API_AUTH_PASS)}
        api_url = settings.API_ENDPOINT_URL+ '/v1/intellicodes/' + str(intellicode) + "/incidents"

        logging.debug('calling api_url ' + api_url)

        # Submitting the incident
        json_response = requests.post(api_url, data=json.dumps(final_data), headers=json_headers)
        json_object = json_response.json()
        try:
            if request.FILES:
                if json_object.get('data'):
                    if json_object['data'].get('incident_id'):
                        incident_id = json_object['data']['incident_id']
                        upload_api_url = settings.API_ENDPOINT_URL + '/v1/intellicodes/' + str(
                            intellicode) + "/incidents/" + str(incident_id)
                        upload_response = upload_to_s3(request, upload_api_url, incident_id)
                        logging.debug('calling api_url ' + api_url)
        except:
            pass

        if json_object['data'].get('after_hour_report_message') is not None:
            template_params["after_hour_report_message"] = json_object['data'].get('after_hour_report_message')

        template_params["name"] = "success"
        template_params["incident_response"] = "Your report has been submitted."
        template_params["contacts"] = request.session['contacts']

        logging.debug(json_object)

        logging.debug('Response Code: ' + str(json_response.status_code))

        if json_response.status_code == 200:
            return render(request, 'incident_result.html', template_params)
        else:
            return render(request, 'incident_result.html', template_params)
    except:
        return render(request, 'options.html', template_params)
