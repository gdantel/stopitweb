from django.conf import settings
from datetime import date
import logging

logging.basicConfig(
    format = '%(asctime)s %(levelname)s: %(message)s',
    filename = settings.LOG_DIR_PATH + '/'\
        + 'appweb-error_log',
    level = logging.DEBUG
)
