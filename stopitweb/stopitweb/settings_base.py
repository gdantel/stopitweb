# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#+hbglue0ht4&40vk)1)o25$ff2ni1xj*pemb6!*23jo$(4jd@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

ROOT_PATH = os.path.dirname(__file__)

INITIAL_KEY = 'bd2530a74w19bc06alxgh8ksbd2530a7'

# Version file updated by Jenkins for cache buster tag
DEPLOY_VERSION_FILE = 'version.txt'

CMS_PROTOCOL = 'https://'
CMS_HOST = 'www.stopitsolutions.'
CDN_HOST_PROTOCOL = 'https://'
CDN_HOST= 'd1nj3dn42gfhow.cloudfront.net/'
CDN_HOST2= 'd1nj3dn42gfhow.cloudfront.net/'
AWS_S3_ACCESS_KEY = ''
AWS_S3_SECRET_KEY = ''
AWS_S3_BUCKET = ''
API_ENDPOINT_URL = 'https://api-mobile.stopitcyberbully.com'
API_AUTH_NAME = ''
API_AUTH_PASS = ''
FILE_UPLOAD_PATH = '/tmp'

SESSION_REDIS_HOST = ''
SESSION_REDIS_PORT = 6379
SESSION_REDIS_DB = 3

IDLE_TIMEOUT_MINUTES = 5

# Messenger Host
MESSENGER_ACTIVE_FLAG = True
MESSENGER_HOST = 'messenger.stopitcyberbully.com'
MESSENGER_SOCKET_PORT = 5222
MESSENGER_HTTP_PROTOCOL = 'https://'
MESSENGER_HTTP_PORT = 7443


SESSION_ENGINE = 'redis_sessions.session'
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_COOKIE_AGE = 1900
FIFTEEN_MINUTES = 0
KEEP_LOGGED_IN_AGE = 180000
SESSION_ALERT_TIMEOUT = 1740

INCIDENT_SOURCE_ID = 2

LOG_DIR_PATH = '/tmp'
LOG_FILE_DATE_FORMAT = '%Y%m%d'
LOG_FILE_EXTENSION = '.log'


# Application definition
INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.auth',
    'onboarding'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'stopitweb.urls'

#static file directory inclusion
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'stopitweb.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {}


CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'TIMEOUT': 2,
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

from django.utils.translation import ugettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('es', _('Spanish')),
    ('fr', _('French')),
    ('ja', _('Japanese')),
)
LANGUAGE_CODE = 'en-us'


LOCALE_PATHS = (
    BASE_DIR + '/locale/',
    os.path.join(ROOT_PATH, 'locale'),
    '/locale/',
    'locale',
)


TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
