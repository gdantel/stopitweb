from onboarding.views import Index, OptionsView, help, language, help_incident, help_contact_list, help_edit_contact
from onboarding.views import accesscode, incident, random_access_code, get_random_access_code
from onboarding.stopit_getcountries import get_countries, get_states, get_orgs, get_org_logininfo
from onboarding.stopit_login import login, logout, messenger_via_login, messenger_history, get_toc, check_toc_acceptance, setlangauage, get_offline_messages_ajax, check_tos_for_logininfo, expire_session, flag_as_read
from onboarding.stopit_incident import fileincident
from onboarding.stopit_contacts import show_contacts, show_contact, save_contact
from django.conf.urls import include, url
from django.views.i18n import javascript_catalog

js_info_dict = {
    'domain':'djangojs',
    'packages': ('onboarding',),
}
urlpatterns = [
        url(r'^$', Index.as_view()),
        url(r'^options/', OptionsView.as_view()),
        url(r'^accesscode/', accesscode.as_view()),
        url(r'^enterlogininfo/(?P<org_name>[^/]*)/', get_org_logininfo),
        url(r'^random_code/(?P<token>[^/]*)/', random_access_code),
        url(r'^get_access_code/(?P<token>[^/]*)/', get_random_access_code),
        url(r'^findcountry/', get_countries),
        url(r'^findorg/', get_orgs),
        url(r'^findstate/', get_states),
        url(r'^login/', login),
        url(r'^lang/', setlangauage),
        url(r'^accept_tos/', login),
        url(r'^expire/', expire_session),
        url(r'^loginviainfo/', check_tos_for_logininfo),
        url(r'^logout/', logout),
        url(r'^incident/', incident.as_view()),
        url(r'^file_incident/', fileincident),
        url(r'^getOfflineMessages/', get_offline_messages_ajax),
        url(r'^messenger/(?P<org_id>[^/]*)/(?P<incident_id>[^/]*)/flag_as_read/', flag_as_read),
        url(r'^messenger/', messenger_via_login),
        url(r'^messenger_history/(?P<incident_id>[^/]*)/?', messenger_history),
        url(r'^manage_contacts/', show_contacts),
        url(r'^edit_contact/', show_contact),
        url(r'^save_contact/', save_contact),
        url(r'^language/', language.as_view()),
        url(r'^help/', help_incident.as_view()),
        url(r'^help_incident/', help_incident.as_view()),
        url(r'^help_contact_list/', help_contact_list.as_view()),
        url(r'^help_edit_contact/', help_edit_contact.as_view()),
        url(r'^terms/', get_toc),
        url(r'^i18n/', include('django.conf.urls.i18n')),
        url(r'^jsi18n/', javascript_catalog, js_info_dict, name='javascript-catalog'),
]
